package lection.three.hw.one;

import lection.three.hw.one.exception.GroupOverflowException;
import lection.three.hw.one.exception.StudentNotFoundException;

import java.util.Arrays;

public class Group {

    private Student[] students = new Student[10];
    private String groupName;

    public Group(String groupName, Student[] students) {
        super();
        this.groupName = groupName;
        this.students = students;
    }

    public Group(String groupName) {
        super();
        this.groupName = groupName;
    }

    public Group() {
        super();
    }

    public static void studentsSort(Student[] students) {
        String aLastName;
        String bLastName;
        Student aStudent;
        Student bStudent;

        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {

                if (students[i] == null) {
                    aLastName = "ZLast";
                } else {
                    aLastName = students[i].getLastName();
                }
                aStudent = students[i];

                if (students[j] == null) {
                    bLastName = "ZLast";
                } else {
                    bLastName = students[j].getLastName();
                }
                bStudent = students[j];

                if (aLastName.compareTo(bLastName) < 0) {
                    students[j] = aStudent;
                    students[i] = bStudent;
                }
            }
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public void addStudent(Student student) throws GroupOverflowException {
        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i] != null && i == this.students.length - 1) {
                throw new GroupOverflowException(student.getName() + " " + student.getLastName() +
                        " cannot be added to the " + this.groupName + " group as it is full!");
            } else if (this.students[i] == null) {
                student.setGroupName(this.groupName);
                this.students[i] = student;
                System.out.println("\u001B[32m" +
                        student.getName() + " " + student.getLastName() +
                        " added to " + this.groupName + " group." + "\u001B[0m");
                break;
            }
        }
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
        Student foundStudent = null;
        for (Student student : this.students) {
            if (student != null && student.getLastName().equals(lastName)) {
                foundStudent = student;
            }
        }
        if (foundStudent == null) {
            throw new StudentNotFoundException("Student with last name " + lastName + " not found " + this.groupName + " group.");
        }

        return foundStudent;
    }

    public boolean removeStudentByID(int id) {
        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i] != null && this.students[i].getId() == id) {
                this.students[i] = null;
                System.out.println("Student with student card id " + id + " is removed from group " + this.groupName);
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        studentsSort(students);
        return "Group{" +
                "groupName='" + groupName + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
