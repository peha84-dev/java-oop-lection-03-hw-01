package lection.three.hw.one;

import lection.three.hw.one.exception.GroupOverflowException;
import lection.three.hw.one.exception.StudentNotFoundException;

public class Main {
    public static void main(String[] args) {

        String groupName = "Journalism";
        Group group = new Group(groupName);

        Student student01 = new Student("Volodymyr", "Petrenko", Gender.MALE, 1, groupName);
        Student student02 = new Student("Vsevolod", "Andriienko", Gender.MALE, 2, groupName);
        Student student03 = new Student("Rostyslav", "Petruk", Gender.MALE, 3, groupName);
        Student student04 = new Student("Liudmyla ", "Andriiuk", Gender.FEMALE, 4, groupName);
        Student student05 = new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName);
        Student student06 = new Student("Vira", "Andriievych", Gender.FEMALE, 6, groupName);
        Student student07 = new Student("Nadiia", "Petriv", Gender.FEMALE, 7, groupName);
        Student student08 = new Student("Liubov", "Andriiv", Gender.FEMALE, 8, groupName);
        Student student09 = new Student("Ihor", "Petrash", Gender.MALE, 9, groupName);
        Student student10 = new Student("Oleh", "Andriiash", Gender.MALE, 10, groupName);
        Student student11 = new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName);

        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        addStudentToGroup(group, student01);
        addStudentToGroup(group, student02);
        addStudentToGroup(group, student03);
        addStudentToGroup(group, student04);
        addStudentToGroup(group, student05);
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        addStudentToGroup(group, student06);
        addStudentToGroup(group, student07);
        addStudentToGroup(group, student08);
        addStudentToGroup(group, student09);
        addStudentToGroup(group, student10);
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");

        try {
            Student foundStudent = group.searchStudentByLastName(student10.getLastName());
            System.out.println("\u001B[33m" + "Found student: " + foundStudent.toString() + "\u001B[0m");
        } catch (StudentNotFoundException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }

        try {
            Student foundStudent = group.searchStudentByLastName(student11.getLastName());
            System.out.println(foundStudent.toString());
        } catch (StudentNotFoundException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }

        try {
            group.addStudent(student11);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }

        String deletedStudentLastName = student10.getLastName();
        String deletedStudentName = student10.getName();

        if (group.removeStudentByID(student10.getId())) {
            System.out.println("\u001B[35m" +
                    "Student " + deletedStudentLastName + " " + deletedStudentName +
                    " removed from the " + group.getGroupName() + " group" + "\u001B[0m");
        } else {
            System.out.println("\u001B[31m" +
                    "Student with ID " + student10.getId() +
                    " is not removed from the " + group.getGroupName() + " group" + "\u001B[0m");
        }

        if (group.removeStudentByID(9999)) {
            System.out.println("\u001B[35m" +
                    "Student " + deletedStudentLastName + " " + deletedStudentName +
                    " removed from the " + group.getGroupName() + " group" + "\u001B[0m");
        } else {
            System.out.println("\u001B[31m" +
                    "Student with ID 9999" +
                    " is not removed from the " + group.getGroupName() + " group" + "\u001B[0m");
        }

        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        addStudentToGroup(group, student11);
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
    }

    private static void addStudentToGroup(Group group, Student student) {
        try {
            group.addStudent(student);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }
    }
}